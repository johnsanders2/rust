# Summary #

A rust playground inside a docker container. The idea is that you can add your projects into the ./projects folder, and then compile/run them in the container.

## Build the project ##

``bash
docker build -t rust .
`

## Run the container with projects mounted to the container ##

``bash
docker run -it --rm -v $(pwd)/projects:/projects rust bash
`
