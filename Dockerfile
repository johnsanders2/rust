FROM ubuntu:16.04
RUN apt-get update && \
	apt-get upgrade -y && \
	apt-get install curl build-essential -y && \
	curl https://sh.rustup.rs -sSf -o /tmp/rustup-installer.sh && \
	chmod +x /tmp/rustup-installer.sh && \
	/tmp/rustup-installer.sh -y && \
	echo 'export PATH="$HOME/.cargo/bin:$PATH"' >> ~/.bashrc

WORKDIR /projects

COPY ./projects /projects
